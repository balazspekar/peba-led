int p = 5;
int e = 3;
int b = 10;
int a = 11;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(p, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(a, OUTPUT);
  
}

// the loop function runs over and over again forever
void loop() {
  randomSeed(analogRead(0));
  int randNumber = random(0, 100);
  if (randNumber > 50) {
    circus();
  } else {
    noCircus();
  }
}

void circus() {
  while (true) {
    simple_blink(300);
    back_and_forth_digital();
    delay(100);
    simple_blink(300);
    one_after_another();
    delay(300);
    simple_blink(300);
    //pulseAll();
    delay(100);
  }
}

void noCircus() {  
  fadeInAllAtOnce(10);
  while (true) {
    delay(60*1000);  
  }

}

void fadeIn(int letter, int speed) {
  for (int i = 0; i <= 255; i+=2) {
      analogWrite(letter, i);
      delay(speed);
  }
}

void fadeOut(int letter, int speed) {
  for (int i = 255; i >= 0; i--) {
      analogWrite(letter, i);
      delay(speed);
  }
}

void completeFadeCylce(int letter, int speed) {
  fadeIn(letter, speed);
  fadeOut(letter, speed);
}

void turnOn(int letter) {
  digitalWrite(letter, HIGH);
}

void turnOff(int letter) {
  digitalWrite(letter, LOW);
}

void completeOnOffCylce(int letter, int speed) {
  turnOn(letter);
  delay(speed);
  turnOff(letter);
}

void fadeInAllAtOnce(int speed) {
  for (int i = 0; i <= 255; i++) {
      analogWrite(p, i);
      analogWrite(e, i);
      analogWrite(b, i);
      analogWrite(a, i);
      delay(speed);
  }
}

void fadeOutAllAtOnce(int speed) {
  for (int i = 255; i >= 0; i--) {
      analogWrite(p, i);
      analogWrite(e, i);
      analogWrite(b, i);
      analogWrite(a, i);
      delay(speed);
  }
}

void pulseAll() {
    fadeInAllAtOnce(5);
    delay(200);
    fadeOutAllAtOnce(5); 
}

void one_after_another() {
  for (int i = 0; i <= 2; i++) { 
    completeFadeCylce(p, 1);
    completeFadeCylce(e, 1);
    completeFadeCylce(b, 1);
    completeFadeCylce(a, 1);
    delay(100);
    fadeInAllAtOnce(3);
    delay(500);
    fadeOutAllAtOnce(3);
    delay(100); 
  }
}

void back_and_forth_digital() {
  for (int i = 0; i <= 2; i++) { 
    completeOnOffCylce(p, 150);
    completeOnOffCylce(e, 150);
    completeOnOffCylce(b, 150);
    completeOnOffCylce(a, 150);
   
    completeOnOffCylce(a, 150);
    completeOnOffCylce(b, 150);
    completeOnOffCylce(e, 150);
    completeOnOffCylce(p, 150);
  }
}

void simple_blink(int speed) {
  for (int i = 0; i <= 2; i++) {
    digitalWrite(p, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(e, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(b, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(a, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(speed);                       // wait for a second
    digitalWrite(p, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(e, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(b, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(a, LOW);    // turn the LED off by making the voltage LOW
    delay(speed);                       // wait for a second
  }
}
